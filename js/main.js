jQuery.fn.extend({
	addNavItem: function(url, title){
		//TODO: check that parent element is a list
	  var newli = document.createElement("li");
	  var newa  = document.createElement("a");
	    
	  $(newa).attr("href",url)
	    .text(title);
		
	  this.append($(newli).append(newa));
	  
	  return $(newli);
	},

	addSubNav: function () {
	  var newul = document.createElement("ul");

	  this.append(newul);
	  
      $(newul).prev().append('<a href="#' + googleSheetID + '" class="accordion-toggle"><span class="tri"><span class="visHid">toggle</span></span></a>');
	  
	  return $(newul);
	}
})

updatePage = function(elmnt) {
	$("a.navLink").removeClass("active");
	var key = elmnt.attr("data-title");
	$("h1").text(key);
	$("p#blurb").html(contents[key]);
	elmnt.addClass("active");
}

var googleSheetsBaseURL = "https://docs.google.com/spreadsheets/d/";
var googleSheetsPostfix = "/edit#gid=0";
var googleSheetID = window.location.hash.split('#')[1] || "1yPv0Iid9XFmY5nZZdBYlTYA-pE2JbV0LaO3xBefbsN8";

var navElement = $("#main-nav"), currentElement = navElement;
var contents = {};

var data;

function handleQueryResponse(response) {
  if (response.isError()){
	if (response.getReasons().some(function(e){return (e == "access_denied" || e == "timeout");})) {
		$("h1").text("Could not access spreadsheet");
		$("p#blurb").html("Please check that you can edit the Google sheet at <a href=" + sheetUrl + ">" + sheetUrl + "</a>.");
	}
	return;
  }

  data = response.getDataTable();

  var primaryPage = "", secondaryPage = "", tertiaryPage = "", quaternaryPage = "";
  var level = 1;
  var thisa;


  for (var i = 1; i < data.getNumberOfRows(); i++) {
	  var oldlevel = level;
	  var currentPage = "";
	  var blurb = data.getValue(i, 4);

	  quaternaryPage = data.getValue(i, 3);

	  if (quaternaryPage !== null) {
		  // current page is fourth-level
		  if (oldlevel <= 3) {
			  currentElement = currentElement.addSubNav();
		  }
		  level = 4; currentPage = quaternaryPage;
	  } else {
		  // not fourth-level, start looking up the tree
		  tertiaryPage = data.getValue(i, 2);

		  if (tertiaryPage !== null) {
			  // current page is third-level
			  if (oldlevel == 2) {
				  // add subnav
				  currentElement = currentElement.addSubNav();
			  } else if (oldlevel == 4) {
				  // go up one level
				  currentElement = currentElement.parent().closest("ul");
			  }
			  level = 3; currentPage = tertiaryPage;
		  } else {
			  secondaryPage = data.getValue(i, 1);

			  if (secondaryPage === null) {
				  primaryPage = data.getValue(i, 0);
				  // current page is primary
				  currentElement = navElement;
				  level = 1; currentPage = primaryPage;
			  } else {
				  //current page is secondary 
				  if (oldlevel == 1) {
					  // add subnav
					  currentElement = navElement.addSubNav();
				  }
				  if (oldlevel == 4) {
					  // go up one level
					  currentElement = currentElement.parent().closest("ul");
					  oldlevel = 3;
				  } // 'else' is intentionally left out
				  if (oldlevel == 3) {
					  // go up one level
					  currentElement = currentElement.parent().closest("ul");
				  }
				  level = 2; currentPage = secondaryPage;
			  }
		  }
	  }

	  thisa = currentElement.addNavItem("#" + googleSheetID, currentPage).find("a");
	  thisa.attr("data-title", currentPage).addClass('navLink');

	  contents[currentPage] = blurb;
  }
  setTimeout(function () {
	  $('ul').hide().eq(0).show();
  }, 10);

  $("a.navLink").click(function () {updatePage($(this));}).eq(0).click();
}

function loadSpreadsheet(url) {
	var query = new google.visualization.Query(url);
	query.send(handleQueryResponse);	
}

var sheetUrl = '';

function updateSpreadsheet(url) {
	navElement.html("")
	sheetUrl = url;
	if (!sheetUrl.startsWith(googleSheetsBaseURL)) { sheetUrl = googleSheetsBaseURL + sheetUrl; }
	if (!sheetUrl.endsWith(googleSheetsPostfix))   { sheetUrl = sheetUrl + googleSheetsPostfix; }
	loadSpreadsheet(sheetUrl);
	$("#google-sheet-link").attr("href", sheetUrl).text(sheetUrl);
}

$("#change-sheet-form button").click(
	function() {
		var newSheetLink = $('#change-sheet-form input').val(); 
		updateSpreadsheet(newSheetLink);
	}
);

$('body').on('click','.accordion-toggle', function(){
	$(this).toggleClass('open').parent().next('ul').slideToggle();
});

updateSpreadsheet(googleSheetsBaseURL + googleSheetID + googleSheetsPostfix);